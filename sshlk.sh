#! /bin/sh

set -o nounset -o errexit
#set -o xtrace

# +
# =================================================================================================
#                                         GLOBAL VARIABLES
# =================================================================================================
# +                                                                                       begin{{{1

export POSIXLY_CORRECT=1

PUBKEY_FILES=''

PUB_FINGERPRINTS=''

LOAD_STATES=''

PUB_COMMENTS=''

NB_ENTRIES=''

KEYS_PATH=~/.ssh/keys

TTL='0'

INDEX_START=''

INDEX_END=''

RELOAD='0'

VAULT_NAME='sshkeys'

VAULT_PATH=''






# +                                                                                         end}}}1
# =================================================================================================
#                                         GLOBAL CONSTANTS
# =================================================================================================
# +                                                                                       begin{{{1

readonly COL_ORANGE="$(tput setaf 3)"

readonly COL_RED="$(tput setaf 1)"

readonly COL_RESET="$(tput sgr0)"

readonly LF="${IFS#??}"

readonly AWK_SCRIPT="$(cat <<-'_EOF_'
	! /---/ { Loaded[$1 $2] = 1; next }
	{
	while(getline) { print ($1 $2 in Loaded)? "*" : " " }
	}
	_EOF_
	)"






# +                                                                                         end}}}1
# =================================================================================================
#                                         FUNCTIONS
# =================================================================================================
# +                                                                                       begin{{{1

# -------------------------------------------------------------------------------------------------
#                                         GENERIC FUNCTIONS
# -------------------------------------------------------------------------------------------------
# +                                                                                       begin{{{2


abort()
  {
  error "$@" '*** ABORTING ***'
  exit 1
  }



error()
  {
  writeln "${COL_RED}$@${COL_RESET}"
  } >&2



warn()
  {
  writeln "${COL_ORANGE}$@${COL_RESET}"
  } >&2



require_cmd()
  {
  for _
  do  command -v "$_" >/dev/null   &&   continue
      warn "missing command: $_"
      return 1
  done
  }



writeln()
  {
  printf '%s\n' "$@"
  }



write()
  {
  printf '%s' "$*"
  }






# +                                                                                         end}}}2
# -------------------------------------------------------------------------------------------------
#                                         SSH FUNCTIONS
# -------------------------------------------------------------------------------------------------
# +                                                                                       begin{{{2

get_pubkeys()
  {
  PUBKEY_FILES="$(find "$KEYS_PATH" -type f -name '*.pub' -printf '%P\0' | sort -z | sed -z -e 's/\\/\\\\/g' -e 's/\n/\\n/g' -e 's/$/\n/')"
  if test -z "$PUBKEY_FILES"
     then error "no keys found under $KEYS_PATH"
     return 1
  else
     PUB_FINGERPRINTS="$(writeln "$PUBKEY_FILES" | xargs -d '\n' printf '%b\x00' | xargs -0 -I '{}' ssh-keygen -l -f "${KEYS_PATH}/{}")"
     PUB_COMMENTS="$(writeln "$PUB_FINGERPRINTS" | sed -e 's/^\S* \S* //' -e 's/ ([^(]*$//')"
     NB_ENTRIES="$(writeln "$PUBKEY_FILES" | wc -l)"
  fi
  }



get_loadedkeys()
  {
  local Output
  local Err

  if Output="$(ssh-add -l   2>&1)"
     then LOAD_STATES="$(writeln "$Output" '---' "$PUB_FINGERPRINTS" | awk --posix -e "$AWK_SCRIPT")"
  elif test "${Err:=$?}" -eq 1
     then LOAD_STATES="$(printf '%s' "$PUB_FINGERPRINTS" | sed -e 's/^.*$/ /')"
  else
     error "$Output" "ssh-add returned error $Err"
     return 1
  fi
  }



add_keys()
  {
  test "$#" -gt 0   ||   return 0
  writeln "$PUBKEY_FILES" | sed -ne "$(printf '%us/\.pub$//p\n' "$@")"          \
                          | xargs -d '\n' -L 1 printf '%s/%b\x00' "$KEYS_PATH"  \
                          | xargs -0 -o ssh-add -t "${TTL:?missing TTL}"
  RELOAD=1
  }



delete_keys()
  {
  if   test "$#" -eq 0
  then ssh-add -D
  else writeln "$PUBKEY_FILES" | sed -ne "$(printf '%up\n' $(seq "$INDEX_START" "$INDEX_END"))"  \
                               | xargs -d '\n' -L 1 printf '%s/%b\x00' "$KEYS_PATH"  \
                               | xargs -0 ssh-add -d
  fi
  RELOAD=1
  }








# +                                                                                         end}}}2
# -------------------------------------------------------------------------------------------------
#                                         USER MENU FUNCTIONS
# -------------------------------------------------------------------------------------------------
# +                                                                                       begin{{{2

check_indices()
  {
  INDEX_START="${1%%-*}"
  INDEX_END="${1#*-}"
  test "$INDEX_START" -le "$INDEX_END"   &&   test "$INDEX_START" -gt 0   &&   test "$INDEX_END" -le "$NB_ENTRIES"   &&   return 0
  warn 'incorrect indices'
  return 1
  }



get_ttl()
  {
  local Plural=''

  test "${TTL%[smhdw]}" != 1   &&   Plural='s'
  case "$TTL" in (0)   write '0 (unlimited)' ;;
                (*m)   write "${TTL%?} minute${Plural}" ;;
                (*h)   write "${TTL%?} hour${Plural}" ;;
                (*d)   write "${TTL%?} day${Plural}" ;;
                (*w)   write "${TTL%?} week${Plural}" ;;
                 (*)   write "${TTL%s} second${Plural}" ;;
  esac
  }



view()
  {
  writeln '' 'PUB_FINGERPRINTS' '----------------'
  writeln "$PUB_FINGERPRINTS" | nl
  writeln 'LOADED KEYS' '-----------'
  ssh-add -l | nl
  }



process_input()
  {
  local Part
  local Output
  local Arg="$1"

  test -n "$Arg"   ||   return 1
  set --
  RELOAD=0

  for Part in $(writeln "$Arg" | tr '[:upper:]' '[:lower:]')
  do  if   expr "$Part" : '\(d\?[0-9]\+\|d\?[0-9]\+-[0-9]\+\|[dqvm]\|t0\|t[1-9][0-9]*[smhdw]\?\|:[^ ]\+\)$' >/dev/null
           then case "$Part" in (q)  # quit
                                     return 1 ;;

                          (t[0-9]*)   # set TTL
                                      add_keys "$@"
                                      TTL="${Part#?}" ;;

                                (d)   # delete all keys from agent
                                      delete_keys ;;

                          (d[0-9]*)   # delete specified keys from agent
                                      check_indices "${Part#?}"   &&   delete_keys "$INDEX_START" "$INDEX_END" ;;

                           ([0-9]*)   # accumulate specified indices
                                      check_indices "$Part"       &&   set -- "$@" $(seq "$INDEX_START" "$INDEX_END") ;;

                                (v)   # view
                                      view ;;

                                (m)   # move and symlink private keys to vault
                                      move_private_keys ;;

                               (:*)   # keyword search
                                      if   Output="$(writeln "$MENU_ITEMS" | grep -i -F -m 1 -n -e "${Part#:}")"
                                           then set -- "$@" "${Output%%:*}"
                                      else warn "keyword \"$Part\" not found"
                                      fi ;;

                                (*)   # unexpected value
                                      abort "unexpected value: \"$Part\"" ;;
                esac
      else warn "invalid directive: \"$Part\""
      fi
  done

  add_keys "$@"
  test "$RELOAD" -eq 1   &&   get_loadedkeys
  return 0
  }



compile_menu_items()
  {
  MENU_ITEMS="$( Files="$PUBKEY_FILES"
                 Comments="$PUB_COMMENTS"

                 for Count in $(seq 1 "$NB_ENTRIES")
                 do  printf '%s %s\n' "${Files%%${LF}*}" "(${Comments%%${LF}*})"
                     Files="${Files#*${LF}}"
                     Comments="${Comments#*${LF}}"
                 done )"
  }



display_menu()
  {
  local MenuItems
  local States
  local Count

  writeln '' "TTL: $(get_ttl)"

  MenuItems="$MENU_ITEMS"
  States="$LOAD_STATES"

  for Count in $(seq 1 "$NB_ENTRIES")
  do  printf '%3u %s %s\n' "$Count" "[${States%%${LF}*}]" "${MenuItems%%${LF}*}"
      MenuItems="${MenuItems#*${LF}}"
      States="${States#*${LF}}"
  done
  }






# +                                                                                         end}}}2
# -------------------------------------------------------------------------------------------------
#                                         CRYPTMOUNT FUNCTIONS
# -------------------------------------------------------------------------------------------------
# +                                                                                       begin{{{2

mount_vault()
  {
  local Output

  if   mountpoint -q "$VAULT_PATH"
       then warn "cryptmount: target \"$VAULT_NAME\" already mounted"
       return 0
  else # give user three attempts to supply the correct password
       for _ in 1 2 3
       do cryptmount --mount "$VAULT_NAME"   &&   return 0
       done
  fi
  return 1
  }



unmount_vault()
  {
  # shellcheck disable=SC2015
  cryptmount -u "$VAULT_NAME"   &&   writeln '' "unmounted \"$VAULT_NAME\""   ||   :
  }



get_vault_path()
  {
  local Output

  VAULT_PATH=''

  if   ! Output="$(cryptmount --list | grep -m 1 -e "^${VAULT_NAME}\\b")"
       then error "cryptmount: unknown target \"$VAULT_NAME\""
  elif test -z "${VAULT_PATH:="$(writeln "$Output" | cut -d \" -f 2)"}"
       then error 'cryptmount: cannot determine mount path from `--list` output'
  elif ! test -e "$VAULT_PATH"
       then error "cryptmount: mount path \"$VAULT_PATH\" does not exist"
  else return 0
  fi
  return 1
  }



move_private_keys()
  (
  cd "$KEYS_PATH"

  # clone subdirectories tree into vault
  find . -type d -printf '%P\0' | xargs -0 -I '{}' mkdir -pv "$VAULT_PATH/{}"

  # copy private keys into vault
  grep -rlFxsZ -m 1 -e '-----BEGIN OPENSSH PRIVATE KEY-----' | xargs -0 -I '{}' cp -nv '{}' "$VAULT_PATH/{}"

  # delete orinal files and create symlinks
  grep -rlFxsZ -m 1 -e '-----BEGIN OPENSSH PRIVATE KEY-----' |  xargs -0 -I '{}' ln -svf "$VAULT_PATH/{}" "$KEYS_PATH/{}"
  )







# +                                                                                         end}}}1
# =================================================================================================
#                                         MAIN SCRIPT
# =================================================================================================
# +                                                                                       begin{{{1

require_cmd 'awk' 'ssh-add' 'sed' 'cryptmount' 'find' 'xargs' 'ssh-keygen'

get_vault_path

trap ':' INT

mount_vault   ||   exit 1

trap unmount_vault EXIT

get_pubkeys   &&   get_loadedkeys   ||   exit 1

compile_menu_items

if   test "$#" -gt 0
     then display_menu
     process_input "$*"   ||   :
else while :
     do    display_menu
           read -p ' > ' _
           process_input "$_"   ||   break
     done
fi

exit 0
